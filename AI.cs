﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour {

    public float Speed;
    public Transform Player;
    public float Xdist;
    public float Zdist;

    public Sprite Walking1;
    public Sprite Walking2;
    private Sprite Walk;

    private Vector3 Target;
    public bool Kill = false;

    public Sprite Idle;

    public Sprite Attack;

    public float Damage = 10;

    private float WalkingTimer = 0;
    public float AttackTimer = 0;
    private float AttackAstTimer = 0;

    public float AttackTime = 2;

    public float AttackDist = 1;

    private void Start()
    {
        Player = Movement.ins;
    }

    void Update ()
    {
        if (Kill)
        {
            transform.GetComponent<SpriteRenderer>().color = new Color(1, 0.9f, 0.9f);
            Target = Player.position;
        }
        else
        {
            transform.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
            if (Vector3.Distance(Target, Player.position) > 10)
            {

                float x = Random.Range(-5, 5);
                float yz = Random.Range(-2, 2);
                Target =  new Vector3(Player.position.x + x, yz, yz);
            }
        }

        if (AttackTimer > 0)
        {
            AttackTimer -= Time.deltaTime;
        }
        if (AttackAstTimer > 0)
        {
            AttackAstTimer -= Time.deltaTime;
            transform.GetComponent<SpriteRenderer>().sprite = Attack;
        }
        else
        {
            transform.GetComponent<SpriteRenderer>().sprite = Idle;
        }

		if (Mathf.Abs(transform.position.x - Target.x) > Xdist)
        {
            WalkCycle();
            if (Mathf.Sign(transform.position.x - Target.x) < 0)
            {
                transform.parent.position += new Vector3(Speed * Time.deltaTime, 0, 0);
                transform.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                transform.parent.position -= new Vector3(Speed * Time.deltaTime, 0, 0);
                transform.transform.rotation = Quaternion.Euler(0, 180, 0);
            }
        }
        else
        {
            if (Mathf.Abs(transform.position.z - Target.z) > Zdist)
            {
                WalkCycle();
                if (Mathf.Sign(transform.position.z - Target.z) < 0)
                {
                    transform.parent.position += new Vector3(0, Speed * Time.deltaTime, Speed * Time.deltaTime);
                }
                else
                {
                   transform.parent.position -= new Vector3(0, Speed * Time.deltaTime, Speed * Time.deltaTime);
                }
            }
        }

        if (Kill && Vector3.Distance(transform.position, Player.position) <= AttackDist && AttackTimer <= 0)
        {
            AttackAstTimer = 0.25f;
            AttackTimer = 1.2f;
            Player.GetComponent<DamageManager>().TakeDamage((Input.GetButton("Block"))?(Damage/4):Damage);
        }
	}

    void WalkCycle()
    {
        print("triggering walk");
        transform.GetComponent<SpriteRenderer>().sprite = Walk;
        if (WalkingTimer <= 0)
        {
            WalkingTimer = 0.3f;
            if (Walk == Walking1)
            {
                Walk = Walking2;
            }
            else
            {
                Walk = Walking1;
            }
        }
        else
        {
            WalkingTimer -= Time.deltaTime;
        }
    }
}
