﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawning : MonoBehaviour {

    public GameObject EnemyPrefab;

    public static List<AI> Enemies = new List<AI>();

	void Update ()
    {
		if (Input.GetKeyDown(KeyCode.Q))
        {
            float x = Random.Range(-20,20);
            float yz = Random.Range(-2, 2);
            Enemies.Add(Instantiate(EnemyPrefab, new Vector3(x, yz, yz), Quaternion.Euler(0,0,0)).transform.GetChild(0).GetComponent<AI>());
        }

        bool anyAttack = false;

        foreach (AI i in Enemies)
        {
            if (i)
            {
                anyAttack = anyAttack || i.Kill;
            }
        }

        if (!anyAttack && Enemies.Count > 0)
        {
            Enemies[Random.Range(0, Enemies.Count - 1)].Kill = true;
        }
	}
}
